"""
Django settings for CSVtoFTPandBack project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""
from __future__ import absolute_import 
from celery.schedules import crontab
import djcelery
from datetime import timedelta
import os


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '&3lme^e$67v8kqxfd$fnd)4etgu$2s$h9o6w#dcc4@w1-e&il+'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = [all]


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'shop',
    'djcelery',
    'kombu.transport.django',
    'storages',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'CSVtoFTPandBack.urls'

WSGI_APPLICATION = 'CSVtoFTPandBack.wsgi.application'

os.environ[ 'DJANGO_SETTINGS_MODULE' ] = "CSVtoFTPandBack.settings"

ADMINS = (
    ('Hussien', 'gorelami@gmail.com')
)

SERVER_EMAIL = os.environ.get('SERVER_EMAIL', 'django-local@toocoomedia.com')


C2FB_LOG_ROOT = os.environ.get("C2FB_LOG_ROOT", "logs")


LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(thread)d %(funcName)s() line: %(lineno)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console':{
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'file': {
            'level': 'DEBUG',
            'class':'logging.FileHandler',
            'filename': os.path.join(C2FB_LOG_ROOT,'C2FB.log'),
            'formatter':'verbose',
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
            'email_backend': 'django.core.mail.backends.smtp.EmailBackend',
        }
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
            'level': 'INFO',
        },
        'django.request': {
            'handlers': ['file', 'console'],
            'level': 'ERROR',
            'propagate': False,
        },
        'csvtoftp': {
            #'handlers': ['console', 'file'],
            'handlers': ['console', 'file'],
            'level': 'DEBUG',
        },
    }
}



# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.environ.get('C2FB_DB_NAME', 'csvtoftpandback'),
        'USER': os.environ.get('C2FB_DB_USERNAME', 'root'),
        'PASSWORD': os.environ.get('C2FB_DB_PASSWORD', '@miR2$12'),
        'HOST': os.environ.get('C2FB_DB_DOMAIN', 'localhost'),   # Or an IP Address that your DB is hosted on
        'PORT': os.environ.get('C2FB_DB_PORT', '3306')
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)



#STATIC_ROOT = os.path.join(BASE_DIR, "static")
STATIC_URL = '/static/'

AWS_STORAGE_BUCKET_NAME = 'csvtoftpandback'
AWS_ACCESS_KEY_ID = 'AKIAJLJXMT73IPQAYSKA'
AWS_SECRET_ACCESS_KEY = '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'

# Tell django-storages that when coming up with the URL for an item in S3 storage, keep
# it simple - just use this domain plus the path. (If this isn't set, things get complicated).
# This controls how the `static` template tag from `staticfiles` gets expanded, if you're using it.
# We also use it in the next setting.
AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME

# This is used by the `static` template tag from `static`, if you're using that. Or if anything else
# refers directly to STATIC_URL. So it's safest to always set it.
STATIC_URL = "https://%s/" % AWS_S3_CUSTOM_DOMAIN

# Tell the staticfiles app to use S3Boto storage when writing the collected static files (when
# you run `collectstatic`).
STATICFILES_STORAGE = 'storages.backends.s3boto.S3BotoStorage'



TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates')]

SHOPIFY_APP_KEY = os.environ.get('FOREWARDS_SHOPIFY_APP_KEY', '3baba39ff25328ad58eea84cbc3fd641')
SHOPIFY_SHARED_SECRET = os.environ.get('FOREWARDS_SHOPIFY_SHARED_SECRET', '76681cc8dd0e661b0ccfd6461759161e')
SHOPIFY_API_SCOPE = ['read_products', 'read_customers', 'read_orders', 'write_script_tags', 'write_themes']



djcelery.setup_loader()
BROKER_URL = 'django://'


# The backend used to store task results - because we're going to be 
# using RabbitMQ as a broker, this sends results back as AMQP messages
#CELERY_RESULT_BACKEND = "amqp"
CELERY_IMPORTS = ("tasks", )
#CELERY_ALWAYS_EAGER = True
#CELERY_ACCEPT_CONTENT = ['json']


# The default Django db scheduler
CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"
CELERYBEAT_SCHEDULE = {
    "scrape-officials": {
        "task": "tasks.ftpTransfer",
        "schedule": timedelta(seconds=5),
    },
}


AVAILABLE_FIELDS = ["line_item.id", "order.customer.email", "order.buyer_accepts_marketing","order.cancel_reason", "order.email", "order.cart_token"]
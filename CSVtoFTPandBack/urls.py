from django.conf.urls import patterns, include, url
from django.contrib import admin
from CSVtoFTPandBack import settings

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^install$', 'shop.views.install'),
    url(r'^shop/finalize/$',  'shop.views.finalize'),
    url(r'^$', 'shop.views.login'),
    url(r'^configuration$', 'shop.views.configuration'),
    url(r'^makecsv$', 'shop.views.createCSV'),
    url(r'^celerytest$', 'shop.view.celeryTest'),
)

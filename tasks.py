from celery import task
from celery import Celery
import logging
import os
from shop.models import LastSchedulerRun, Configuration, Shop
from django.utils import timezone

logger = logging.getLogger('csvtoftp')

import django
django.setup()

@task()
def add(x,y):
    print "testing" 
    return (x + y)

@task()
def ftpTransfer():
    
    logger.info("Scheduler is Running")
    
    lastSchedulerRun = LastSchedulerRun.objects.filter()
    
    if not lastSchedulerRun:
        new_lastSchedulerRun = LastSchedulerRun()
        new_lastSchedulerRun.lastRunDate = timezone.now()
        new_lastSchedulerRun.save()
        
        
    # Let's go through all the shop and see if it's time for them to run
    shops   = Shop.objects.filter()
    
    if shops:
        for shop in shops:
            if shop.is_installed:
                config = Configuration.objects.filter(shop = shop).get(0)
                
                if not config:
                    logger.error("Shop " + str(shop.name) +" has no config, but it should")
                
                if config.isEnabled:
                    # see if the shop is scheduled
                
    
    
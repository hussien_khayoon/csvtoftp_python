from django.db import models
from shop.model_choices import TransferScheduleTypes

# Create your models here.
class Shop(models.Model):
    domain                    = models.CharField(max_length=255, unique=True, db_column='shop_url')
    name                      = models.CharField(max_length=255, db_column='shop_name')
    shop_email                = models.CharField(max_length=255)
    access_token              = models.CharField(max_length=255)
    login_cookie              = models.CharField(max_length=255)
    is_installed              = models.BooleanField(default=True)
    created_on                = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_on                = models.DateTimeField(auto_now=True, auto_now_add=True)
    recurring_charge_id       = models.IntegerField(null=True, blank=True)
    

class Configuration(models.Model):
    shop                       = models.ForeignKey('Shop')
    ftpServer                  = models.CharField(max_length=255)
    ftpUser                    = models.CharField(max_length=255)
    ftpPassword                = models.CharField(max_length=255)
    ftpPort                    = models.CharField(max_length=255)
    ftpPath                    = models.CharField(max_length=255)
    isEnabled                  = models.BooleanField(default=True)
    lastTransferDate           = models.DateTimeField(null=True)  
    lastSuccessfulTransferDate = models.DateTimeField(auto_now=True, auto_now_add=True)
    lastStatus                 = models.CharField(max_length=255)
    transferScheduleValue      = models.IntegerField(null=True, choices=TransferScheduleTypes.choices() )
    orderFields                = models.CharField(max_length=2550)
    
class LastSchedulerRun(models.Model):
    lastRunDate                = models.DateTimeField()
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='LastSchedulerRun',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('lastRunDate', models.DateTimeField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RenameField(
            model_name='configuration',
            old_name='FtpPassword',
            new_name='ftpPassword',
        ),
        migrations.RenameField(
            model_name='configuration',
            old_name='FtpPath',
            new_name='ftpPath',
        ),
        migrations.RenameField(
            model_name='configuration',
            old_name='FtpPort',
            new_name='ftpPort',
        ),
        migrations.RenameField(
            model_name='configuration',
            old_name='FtpServer',
            new_name='ftpServer',
        ),
        migrations.RenameField(
            model_name='configuration',
            old_name='FtpUser',
            new_name='ftpUser',
        ),
        migrations.RenameField(
            model_name='configuration',
            old_name='IsEnabled',
            new_name='isEnabled',
        ),
        migrations.RenameField(
            model_name='configuration',
            old_name='LastStatus',
            new_name='lastStatus',
        ),
        migrations.RenameField(
            model_name='configuration',
            old_name='LastSuccessfulTransferDate',
            new_name='lastSuccessfulTransferDate',
        ),
        migrations.RenameField(
            model_name='configuration',
            old_name='LastTransferDate',
            new_name='lastTransferDate',
        ),
        migrations.RenameField(
            model_name='configuration',
            old_name='OrderFields',
            new_name='orderFields',
        ),
        migrations.RenameField(
            model_name='configuration',
            old_name='TransferScheduleValue',
            new_name='transferScheduleValue',
        ),
    ]

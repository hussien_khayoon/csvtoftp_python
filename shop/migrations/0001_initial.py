# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Configuration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('FtpServer', models.CharField(max_length=255)),
                ('FtpUser', models.CharField(max_length=255)),
                ('FtpPassword', models.CharField(max_length=255)),
                ('FtpPort', models.CharField(max_length=255)),
                ('FtpPath', models.CharField(max_length=255)),
                ('IsEnabled', models.BooleanField(default=True)),
                ('LastTransferDate', models.DateTimeField(null=True)),
                ('LastSuccessfulTransferDate', models.DateTimeField(auto_now=True, auto_now_add=True)),
                ('LastStatus', models.CharField(max_length=255)),
                ('TransferScheduleValue', models.IntegerField(null=True, choices=[(4, b'CUSTOM'), (3, b'DAILY'), (0, b'HOURLY'), (1, b'SIX_HOURS'), (2, b'TWICE_DAILY')])),
                ('OrderFields', models.CharField(max_length=2550)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Shop',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('domain', models.CharField(unique=True, max_length=255, db_column=b'shop_url')),
                ('name', models.CharField(max_length=255, db_column=b'shop_name')),
                ('shop_email', models.CharField(max_length=255)),
                ('access_token', models.CharField(max_length=255)),
                ('login_cookie', models.CharField(max_length=255)),
                ('is_installed', models.BooleanField(default=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True, auto_now_add=True)),
                ('recurring_charge_id', models.IntegerField(null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='configuration',
            name='shop',
            field=models.ForeignKey(to='shop.Shop'),
            preserve_default=True,
        ),
    ]

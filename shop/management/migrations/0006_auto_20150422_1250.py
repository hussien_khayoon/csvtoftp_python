# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0005_configuration_orderfields'),
    ]

    operations = [
        migrations.AlterField(
            model_name='configuration',
            name='TransferScheduleValue',
            field=models.IntegerField(null=True, choices=[(4, b'CUSTOM'), (3, b'DAILY'), (0, b'HOURLY'), (1, b'SIX_HOURS'), (2, b'TWICE_DAILY')]),
            preserve_default=True,
        ),
    ]

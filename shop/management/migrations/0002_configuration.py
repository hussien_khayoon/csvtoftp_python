# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Configuration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('FtpServer', models.CharField(max_length=255)),
                ('FtpUser', models.CharField(max_length=255)),
                ('FtpPassword', models.CharField(max_length=255)),
                ('FtpPort', models.CharField(max_length=255)),
                ('FtpPath', models.CharField(max_length=255)),
                ('IsEnabled', models.BooleanField(default=True)),
                ('TransferScheduleValue', models.IntegerField(null=True, choices=[(3, b'CUSTOM'), (2, b'DAILY'), (0, b'HOURLY'), (1, b'SIX_HOURS')])),
                ('shop', models.ForeignKey(to='shop.Shop')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]

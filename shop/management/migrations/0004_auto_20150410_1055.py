# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0003_auto_20150409_1722'),
    ]

    operations = [
        migrations.AlterField(
            model_name='configuration',
            name='LastSuccessfulTransferDate',
            field=models.DateTimeField(auto_now=True, auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='configuration',
            name='LastTransferDate',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0004_auto_20150410_1055'),
    ]

    operations = [
        migrations.AddField(
            model_name='configuration',
            name='OrderFields',
            field=models.CharField(default='order.email', max_length=2550),
            preserve_default=False,
        ),
    ]

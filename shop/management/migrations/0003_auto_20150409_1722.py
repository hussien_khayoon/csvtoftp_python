# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0002_configuration'),
    ]

    operations = [
        migrations.AddField(
            model_name='configuration',
            name='LastStatus',
            field=models.CharField(default='test', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='configuration',
            name='LastSuccessfulTransferDate',
            field=models.DateTimeField(default=datetime.datetime(2015, 4, 9, 21, 22, 25, 338000, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='configuration',
            name='LastTransferDate',
            field=models.DateTimeField(default=datetime.datetime(2015, 4, 9, 21, 22, 29, 744000, tzinfo=utc)),
            preserve_default=False,
        ),
    ]

from django.shortcuts import render_to_response, redirect
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.conf import settings as django_settings
import shopify
import logging
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseNotFound, HttpResponse
from shop.models import Shop, Configuration
from shop.model_choices import TransferScheduleTypes
import csv
import uuid
import datetime
import ftplib
import os
import traceback
from tasks import add
logger = logging.getLogger('csvtoftp')


# This is the entry point for manually installing the application on your shopify store.
@csrf_exempt
def install(request):
    # The user enters their shopify store name, we get it from the 'shop' variable in the header
    if request.method == 'POST':
        shopify_domain_name = request.REQUEST.get('shop')
        
        # if the shop name is entered the OAuth2 process beings.
        # we build our permission_url using our shopify shared, secret keys and our shop name.
        # we then redirect to that permission_url, which leads us to the login() method
        if shopify_domain_name:
            shopify.Session.setup(api_key=django_settings.SHOPIFY_APP_KEY, secret=django_settings.SHOPIFY_SHARED_SECRET)
            session         = shopify.Session(shopify_domain_name.strip())            
            redirect_uri    = request.build_absolute_uri(reverse('shop.views.finalize'))
            permission_url  = session.create_permission_url(django_settings.SHOPIFY_API_SCOPE, redirect_uri)

            logger.debug(permission_url)
            return redirect(permission_url)
    
        return HttpResponseNotFound("No shop name found.")
    
        
    return render_to_response('shop/install.phtml',
    context_instance=RequestContext(request))


# shopify redirect customer here when user agrees to install (redirect from permission_url)
def finalize(request):
    logger.debug("finalize() called")
    # collect shop, code, timestamp, signature
    params = {}
    params['shop']      = request.REQUEST.get('shop','')
    params['code']      = request.REQUEST.get('code','')
    params['timestamp'] = request.REQUEST.get('timestamp','')
    params['signature'] = request.REQUEST.get('signature','')
    params['hmac']      = request.REQUEST.get('hmac','')

    
    if params['code'] == '':
        logger.error("no temp toekn found")
        return HttpResponseNotFound("No temp token found, cannot process")
    
    # we create a new Shopify session 
    session = shopify.Session(params['shop'])
    
    # we use that session and the url params to request our permanent access token
    try:
        token = session.request_token(params)
    except Exception:
        logger.error("Could not get permanent access token", exc_info = True)
        return HttpResponseNotFound("Could not get permanent access token")
        
    # we check to see if the shop is already installed
    shop = Shop.objects.filter(domain = params['shop'])
    
    # if the shop is already installed we start a django session with our shop
    # this will help us grab the right access token for the shop from the db
    if shop:
        request.session['shop'] = params['shop']
        shop[0].installed = True
        # shop[0].updated...
        shop[0].access_token = token
        shop[0].save()
    else:
        # Here, we do our first shopify API call to get our shop name
        session = shopify.Session(params['shop'], token)
        shopify.ShopifyResource.activate_session(session)
        shopify_shop    = shopify.Shop.current()
        if not shopify_shop:
            logger.error("shopify api call failed to return the shop")

        # we fill our db with our new shop info
        new_shop = Shop()
        new_shop.domain = params['shop']
        new_shop.name = shopify_shop.name
        new_shop.shop_email = shopify_shop.email
        new_shop.access_token = token
        new_shop.is_installed = True
        new_shop.save()
        
        # we also fill our a default configuration profile for our new shop
        new_config                       = Configuration()
        new_config.shop                  = new_shop
        new_config.OrderFields           = "order.email,line_item.id,order.customer.email"
        new_config.save()
        
        request.session['shop'] = params['shop']
    return redirect(reverse('shop.views.configuration'))


# Shopify app entry point, i.e. customer click on CSVtoFTPandBack icon from their shopify admin/apps page
def login(request):
    logger.debug("login() called")
    shop_url        = request.REQUEST.get('shop')

    params = {}
    params['shop']      = request.REQUEST.get('shop','')
    params['timestamp'] = request.REQUEST.get('timestamp','')
    params['signature'] = request.REQUEST.get('signature','')
    params['hmac']      = request.REQUEST.get('hmac','')
    
    # we create a new Shopify session 
    print shop_url
    shopify.Session.setup(secret=django_settings.SHOPIFY_SHARED_SECRET)
    shopify_session = shopify.Session(shop_url)
    verified        = shopify_session.validate_params(params)
    
    if not verified:
        logger.debug('not verified')
        return render_to_response('shop/need_login.phtml')  # signature not correct, redirect to a page show error
    
    elif Shop.objects.filter(domain=shop_url, is_installed=True).count() == 0:
        logger.debug('shop not in db')
        return redirect(reverse('shop.views.install')) #shop not in db, redirect to install page
    else:
        shop                = Shop.objects.filter(domain=shop_url)[0]
        shop.login_cookie   = uuid.uuid4()   
        shop.save()
        request.session['shop'] = shop_url
  
    return redirect(reverse('shop.views.configuration'))

# configuration page
# TODO: work on getting user inputed (note the complexity in the order fields)
def configuration(request):
    # grab the shop name from the active session
    shop_name = request.session['shop']
    shop = Shop.objects.filter(domain = shop_name).first()
    config = Configuration.objects.filter(shop=shop).first()
    
    # there should be a config at this point
    if not config: 
        logger.error("no config found for shop")
    
    if request.method == 'POST':
        config.FtpServer                = request.POST.get('FtpServer','')
        config.FtpUser                  = request.POST.get('FtpUser','')
        config.FtpPassword              = request.POST.get('FtpPassword','')
        config.FtpPath                  = request.POST.get('FtpPath','')
        config.FtpPort                  = request.POST.get('FtpPort','')
        config.TransferScheduleValue    = request.POST.get('TransferScheduleValue','')
        
        isEnabled = request.POST.get('IsEnabled','')
        if isEnabled == 'true': 
            isEnabled = True
        else:
            isEnabled = False
            
        config.IsEnabled                = isEnabled
        
        print request.REQUEST
               
        # newFieldList is a key with multiple values and it's then
        # converted into a comma separated string for the db
        newFieldList =  request.POST.getlist('newFieldList')
        config.OrderFields = ','.join(map(str, newFieldList)) 
        
        config.save()
    
    chosenFields = config.OrderFields
    chosenFields = chosenFields.split(',')
    
    availableFields = django_settings.AVAILABLE_FIELDS

    for field in chosenFields:
        if field in availableFields: 
            availableFields.remove(field)
    
   
    if config.IsEnabled == True:
        config.IsEnabled = 'true'
    else:
        config.IsEnabled = 'false'
    
    return render_to_response('shop/configuration.phtml', {'config': config, 'chosenFields': chosenFields, 
                                                           'availableFields': availableFields},
    context_instance=RequestContext(request))
    
# Here we are writing to the CSV file from the Shopify Orders API Call
#TODO: give user control of these order fields from main configuration page
#TODO: send this csv file to an ftp endpoint using another method   
def createCSV(request):
    shop_name = request.session['shop']
    shop = Shop.objects.filter(domain = shop_name).first()
    orders = {}
    if not shop:
        logger.error("something went wrong here")
    else:
        # This is where we make our order API call to the shop
        session = shopify.Session(shop_name, shop.access_token)
        shopify.ShopifyResource.activate_session(session)
        orders = shopify.Order.find(limit=250)
       
        # Here we grab the order fields that we want to display on the csv file
        config = Configuration.objects.filter(shop=shop).first()
        chosenFields = config.OrderFields
        chosenFields = chosenFields.split(",")
        t = datetime.datetime.now()
        csvfile = "Orders_" + t.strftime('%m_%d_%Y') + ".csv"
        
        
        try:
            with open(csvfile, "w") as output:
                writer = csv.DictWriter(output, fieldnames = chosenFields, delimiter = ',')
                writer.writeheader()
                writer = csv.writer(output, lineterminator='\n')
                for order in orders:
                    for line_item in order.line_items:
                        writelist = []
                        for field in chosenFields:
                            val = eval(field)
                            writelist.append(val)
                    writer.writerow(writelist)

            transferFTP(csvfile, config)   

        except:
            logger.error('Failed to create csv file', exc_info=True)
            
            
            
    return render_to_response('shop/home.phtml', {'orders': orders},
                              context_instance=RequestContext(request))

def transferFTP(filename, config):
    try:      
        ftp = ftplib.FTP(config.FtpServer)
        ftp.login(config.FtpUser, config.FtpPassword)
        ftp.cwd("/")
        os.chdir(r"C:\\Users\\Hussien\\CSVtoFTPandBack")
        ftp.storlines("STOR " + filename, open(filename, 'r'))
    
    except:
        logger.error('Failed to send csv file', exc_info=True)
    
    